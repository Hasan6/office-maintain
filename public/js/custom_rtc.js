var connection = new RTCMultiConnection();
var designer = new CanvasDesigner();
var chkRecordConference = true;
var baseUrl = window.location.href.split("/chat/");
var chatArray = [];
var allMsg = "";
var muteState = true;

$(document).ready(function () {
    // first step, ignore default STUN+TURN servers
    connection.iceServers = [];
    connection.iceServers = iceServers;
    // hide doctors field to patient
    if (isPatients == true) {
        $(".doctor-field").css("display", "none");
        $("#chatBox").css("height", "calc(100vh - 145px");
    }
});

// this line is VERY_important
connection.socketURL = "https://rtcmulticonnection.herokuapp.com:443/";

// connect to socket on disconnect and join to the existing room
connection.onSocketDisconnect = function (event) {
    $("#videoContainer").append(
        "<div class='connection-lost-msg'><b>Connection lost</b><br><i class='fas fa-wave-square'></i><br>reconnecting...</div>"
    );
    // parameter passed 0 since no user conneceted as sockect disconnected
    setVideoElementStyle(0);
    // remove all remote video element from UI as socket disconnected
    $("#videoContainer").children().filter("video").each(function () {
        // console.log(sessionStorage.getItem("localVideoId"), this.id)
        if (sessionStorage.getItem("localVideoId") != this.id) {
            this.pause(); // can't hurt
            delete this; // @sparkey reports that this did the trick (even though it makes no sense!)
            $(this).remove(); // this is probably what actually does the trick
        }
    });
    // console.log("socket.io connection is closed in custom...");
    // reconnect to socket and generate video element again
    connection.connectSocket(function () {
        // console.log('Successfully connected to socket.io server...');
        setTimeout(function () {
            if (sessionStorage.getItem("roomId") != null) {
                roomSessionExist();
                $("#videoContainer").children().filter(".connection-lost-msg").each(function () {
                    $(this).remove();
                });
            }
        }, 1000)
    });
}

// if you want audio+video+chat conferencing
connection.session = {
    audio: true,
    video: true,
    data: true,
};

// set getUserMedia for different browser
navigator.getUserMedia = navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia;

// check user media is available or not
function getMedia() {
    if (navigator.getUserMedia) {
        navigator.getUserMedia(connection.session,
            function (stream) {
                // console.log("get user media: ", stream);
            },
            function (err) {
                // console.log("The following error occurred: " + err);
                alert("There is problem with microphone and camera or used by other application!");
            }
        );
    } else {
        console.log("getUserMedia not supported");
    }
}

connection.sdpConstraints.mandatory = {
    OfferToReceiveAudio: true,
    OfferToReceiveVideo: true,
};

// required canvas page here
designer.widgetHtmlURL = baseUrl[0] + '/public/js/widget.html';
designer.widgetJsURL = "widget.min.js";

// default selected tool in whiteboard
designer.setSelected("pencil");

// list of tools in whiteboard
designer.setTools({
    pencil: true,
    text: true,
    image: true,
    pdf: true,
    eraser: true,
    line: true,
    arrow: true,
    dragSingle: true,
    dragMultiple: true,
    arc: true,
    rectangle: true,
    quadratic: false,
    bezier: true,
    marker: true,
    zoom: false,
    lineWidth: false,
    colorsPicker: false,
    extraOptions: false,
    code: false,
    undo: true,
});

// get time function
function getTime(timeFor) {
    if (timeFor == "chat") {
        var date = new Date();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? "pm" : "am";
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? "0" + minutes : minutes;
        var time = hours + ":" + minutes + " " + ampm;
        return time;
    } else if (timeFor == "meeting") {
        var date = new Date();
        var day = date.getDate();
        var month = date.getMonth();
        var year = date.getFullYear();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        var currentTime = month + "/" + day + "/" + year + " " + hours + ":" + minutes + ":" + seconds;
        return currentTime;
    }
}

// return number of user connected and enable the spme input/button on more than one connected user
function numberOfUsers() {
    // console.log(connection.getAllParticipants());
    var usersConnectedlist = connection.getAllParticipants();
    var usersConnected = usersConnectedlist.length;
    userList(usersConnectedlist, usersConnected);
    // do some task based on user connected
    if (usersConnected > 0) {
        $("#complains").prop("disabled", false);
        $("#diagnosis").prop("disabled", false);
        $("#investigation").prop("disabled", false);
        $("#medicine").prop("disabled", false);
        $("#sendMessageBtn").prop("disabled", false);
        $("#message").prop("disabled", false);
        $("#sendMessageBtnRes").prop("disabled", false);
        $("#messageRes").prop("disabled", false);
    } else {
        $("#sendMessageBtn").prop("disabled", true);
        $("#message").prop("disabled", true);
        $("#sendMessageBtnRes").prop("disabled", true);
        $("#messageRes").prop("disabled", true);
        $("#complains").prop("disabled", true);
        $("#diagnosis").prop("disabled", true);
        $("#investigation").prop("disabled", true);
        $("#medicine").prop("disabled", true);
    }
    return usersConnected;
}

// list out all connected user
function userList(usersConnectedlist, usersConnected) {
    if (usersConnected == 0) {
        $(".user-list").empty();
        $(".user-list").append(
            "Nobody joined the room."
        );
        $("#userConnectedModalBtn").css("display", "none");
    } else if (usersConnected > 0) {
        $(".user-list").empty();
        // list all connected user
        usersConnectedlist.forEach(function (element) {
            // console.log(connection.peers[element]);
            var userExtraInfo = connection.peers[element].extra;
            $(".user-list").append(
                '<div class="media mb-3"><img class="mr-3" src="https://www.enigmatixmedia.com/pics/demo.png" alt="Generic placeholder image"><div class="media-body">' +
                userExtraInfo.fullName +
                "</div></div>"
            );
        });
        $("#userConnectedModalBtn").css("display", "inline");
        $("#userConnectedModalBtn").html(
            '<i class="fas fa-network-wired"></i> <span>' +
            usersConnected +
            '</span> <i class="fas fa-angle-down"></i>'
        );
    }
}

// input value saved in session for doctors field
function storeInSession(e) {
    var inputId = e.srcElement.id;
    sessionStorage.setItem(e.srcElement.id, $("#" + inputId).val());
    // console.log(sessionStorage);
}

// show meeting duration
setInterval(function () {
    if (sessionStorage.getItem("roomId") != null) {
        var currentTime = getTime("meeting");
        var startTime = sessionStorage.getItem("meetingStartTime");
        var diff = moment.duration(moment(currentTime).diff(moment(startTime)));
        var formatedData = [diff.asHours().toFixed(0), diff.minutes(), diff.seconds()].join(':');
        // console.log(startTime, currentTime, diff);
        $("#duration").css("display", "inline");
        $("#duration").html('<i class="fas fa-stopwatch"></i> ' + formatedData);
    }
}, 1000);

// user on leave
connection.onleave = function (event) {
    // console.log("who leave: ", event, connection.isInitiator)
    var remoteUserFullName = event.extra.fullName;
    customAlert(remoteUserFullName)
    setTimeout(function () {
        var usersConnected = numberOfUsers();
        setVideoElementStyle(usersConnected);
        // console.log("after -> participent after leave someone: ", connection.getAllParticipants());
    }, 1000)
}

// alert of which user leave
function customAlert(msg) {
    $("body").append(
        "<div class='custom-alert-active'><b>" + msg + "</b> left the room</div>"
    )
    setTimeout(function () {
        $(".custom-alert-active").addClass("custom-alert-inactive");
        $(".custom-alert-active").removeClass("custom-alert-active");
    }, 3000)
}

// check room id in session storage and join room
function roomSessionExist() {
    $("#chatBox")
        .children()
        .filter(".single-chat-div")
        .each(function () {
            $(this).remove();
        });
    var roomId = sessionStorage.getItem("roomId");
    var nickname = sessionStorage.getItem("nickname");
    // insert extra info of user
    connection.extra = {
        fullName: nickname,
    };
    connection.openOrJoin(roomId);
    document.getElementById("roomIdDiv").innerText = roomId;
    $("#complains").val(sessionStorage.getItem("complains"));
    $("#diagnosis").val(sessionStorage.getItem("diagnosis"));
    $("#investigation").val(sessionStorage.getItem("investigation"));
    $("#medicine").val(sessionStorage.getItem("medicine"));
    $("#followUpDate").val(sessionStorage.getItem("followUpDate"));
    chatArray = JSON.parse(sessionStorage.getItem("chatArray"))
    // console.log(JSON.parse(sessionStorage.getItem("chatArray")));
    if (chatArray != null) {
        chatArray.forEach(function (params) {
            $("#chatBox").append(params);
        });
    }
    // $("#createRoomModalBtn").prop("disabled", true);
    // $("#joinRoomModalBtn").prop("disabled", true);
}

$(document).ready(function () {
    // if session exxist then join room
    if (sessionStorage.getItem("roomId") != null) {
        roomSessionExist();
    } else {
        // console.log(appointmentId, roomId, nickname);
        connection.openOrJoin(roomId);
        sessionStorage.setItem("roomId", roomId);
        sessionStorage.setItem("appointmentId", appointmentId);
        sessionStorage.setItem("nickname", nickname);
        sessionStorage.setItem("meetingStartTime", getTime("meeting"));
        $("#roomIdDiv").text(roomId);
        // insert extra info of user
        connection.extra = {
            fullName: nickname,
        };
    }

    // // show room id field on checkbox checked or not
    // $("#createRoomCheckBtn").click(function () {
    //     if ($("#createRoomCheckBtn").is(":checked")) {
    //         $("#roomIdInputDiv").css("display", "none");
    //     } else {
    //         $("#roomIdInputDiv").css("display", "unset");
    //     }
    // });

    // // submit the form to create a room
    // $("#createRoomBtn").click(function () {
    //     var roomIdInputValue = $("#roomId").val();
    //     var withRandomId = $("#createRoomCheckBtn").is(":checked");
    //     var nickname = $("#nicknameInput").val();
    //     if (roomIdInputValue == "" && withRandomId != true) {
    //         $("#roomId").css("border-color", "red");
    //     } else if (roomIdInputValue == "" && withRandomId == true) {
    //         var roomIdDiv = document.getElementById("roomIdDiv");
    //         var roomId = (roomIdDiv.innerText = connection.token());
    //         // insert extra info of user
    //         connection.extra = {
    //             fullName: nickname,
    //         };
    //         connection.openOrJoin(roomId);
    //         sessionStorage.setItem("roomId", roomId);
    //         sessionStorage.setItem("nickname", nickname);
    //         sessionStorage.setItem("meetingStartTime", getTime("meeting"));
    //         $("#createRoomModal").modal("toggle");
    //         $("#createRoomModalBtn").prop("disabled", true);
    //     } else if (roomIdInputValue != "" && withRandomId != true) {
    //         var roomIdDiv = document.getElementById("roomIdDiv");
    //         roomIdDiv.innerText = roomIdInputValue;
    //         // insert extra info of user
    //         connection.extra = {
    //             fullName: nickname,
    //         };
    //         connection.openOrJoin(roomIdInputValue);
    //         sessionStorage.setItem("roomId", roomIdInputValue);
    //         sessionStorage.setItem("nickname", nickname);
    //         sessionStorage.setItem("meetingStartTime", getTime("meeting"));
    //         $("#createRoomModal").modal("toggle");
    //         $("#createRoomModalBtn").prop("disabled", true);
    //         $("#joinRoomModalBtn").prop("disabled", true);
    //     }
    //     $("#roomId").val("");
    //     $("#createRoomCheckBtn").prop("checked", false);
    //     $("#nicknameInput").val("");
    // });

    // // join room button
    // $("#joinRoomBtn").click(function () {
    //     var roomIdInputValue = $("#roomIdToJoin").val();
    //     var nickname = $("#nicknameInputToJoin").val();
    //     if (roomIdInputValue == "") {
    //         $("#roomIdToJoin").css("border-color", "red");
    //     } else if (roomIdInputValue != "") {
    //         var roomIdDiv = document.getElementById("roomIdDiv");
    //         roomIdDiv.innerText = roomIdInputValue;
    //         // insert extra info of user
    //         connection.extra = {
    //             fullName: nickname,
    //         };
    //         connection.openOrJoin(roomIdInputValue);
    //         sessionStorage.setItem("roomId", roomIdInputValue);
    //         sessionStorage.setItem("nickname", nickname);
    //         sessionStorage.setItem("meetingStartTime", getTime("meeting"));
    //         $("#joinRoomModal").modal("toggle");
    //         $("#createRoomModalBtn").prop("disabled", true);
    //         $("#joinRoomModalBtn").prop("disabled", true);
    //     }
    // });
});

// append video element on streaming video client
connection.onstream = function (event) {
    // console.log(event);
    // video container selector
    var video_container = document.getElementById("videoContainer");
    var video = event.mediaElement;
    // get connected user number
    var usersConnected = numberOfUsers();
    // console.log("number of user connected:", usersConnected);
    $(".bottom-call-bar > button").css("display", "inline-block");
    // console.log(connection.extra);
    if (event.type == "local") {
        sessionStorage.setItem("localVideoId", event.streamid);
        $(".local-video").append(video);
        var vids = $("video");
        // console.log(vids);
        $.each(vids, function () {
            this.controls = false;
        });
    } else {
        video_container.appendChild(video);
        setVideoElementStyle(usersConnected);
    }
    // recording rtc
    if (chkRecordConference === true) {
        var recorder = connection.recorder;
        if (!recorder) {
            recorder = RecordRTC([event.stream], {
                type: 'video'
            });
            recorder.startRecording();
            connection.recorder = recorder;
        } else {
            recorder.getInternalRecorder().addStreams([event.stream]);
        }
        if (!connection.recorder.streams) {
            connection.recorder.streams = [];
        }
        connection.recorder.streams.push(event.stream);
        //   recordingStatus.innerHTML = 'Recording ' + connection.recorder.streams.length + ' streams';
    }
    //// end recording
};

// session record end method
function stopRTCrecord() {
    //console.log(event);
    var recorder = connection.recorder;
    if (!recorder) return alert('No recorder found.');
    recorder.stopRecording(function () {
        var blob = recorder.getBlob();
        invokeSaveAsDialog(blob);
        connection.recorder = null;
    });
};

// set video elemet style
function setVideoElementStyle(usersConnected) {
    var w = $("#videoContainer").innerWidth();
    var h = $("#videoContainer").innerHeight();
    var containerDimension = Math.min(w, h) * Math.min(w, h);
    var numberOfTiles = containerDimension / (usersConnected);
    var videoDimension = Math.floor(Math.sqrt(numberOfTiles));
    // console.log(w, h, containerDimension, numberOfTiles, videoDimension);
    $("#videoContainer>video").css({ height: videoDimension, width: videoDimension });
    var vids = $("video");
    // console.log(vids);
    $.each(vids, function () {
        this.controls = false;
    });
}

// leave the chat room function
function leaveChat() {
    if (window.confirm('Really want to end the session?')) {
        // disconnect with all users
        connection.getAllParticipants().forEach(function (pid) {
            connection.disconnectWith(pid);
        });
        // stop all local cameras
        connection.attachStreams.forEach(function (localStream) {
            // console.log(localStream);
            localStream.stop();
        });
        connection.closeSocket();
        $("#videoContainer").children().filter("video").each(function () {
            this.pause(); // can't hurt
            delete this; // @sparkey reports that this did the trick (even though it makes no sense!)
            $(this).remove(); // this is probably what actually does the trick
        });
        $("#roomIdDiv").text("NO ROOM JOINED");
        $("#createRoomModalBtn").prop("disabled", false);
        $("#joinRoomModalBtn").prop("disabled", false);
        $(".bottom-call-bar > button").css("display", "none");
        $("#userConnectedModalBtn").css("display", "none");
        $("#duration").css("display", "none");
        $("#sendMessageBtn").prop("disabled", true);
        $("#message").prop("disabled", true);
        $("#sendMessageBtnRes").prop("disabled", true);
        $("#messageRes").prop("disabled", true);
        $("#videoContainer").children().filter(".connection-lost-msg").each(function () {
            $(this).remove();
        })
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var appointmentId = sessionStorage.getItem("appointmentId");
        var complains = $("#complains").val();
        var diagnosis = $("#diagnosis").val();
        var investigation = $("#investigation").val();
        var medicine = $("#medicine").val();
        var followUpDate = $("#followUpDate").val();
        var spentHour = $("#duration").text();
        // console.log(appointmentId, diagnosis, medicine, investigation);
        if (isPatients == false) {
            $.ajax({
                method: "POST",
                dataType: 'JSON',
                url: baseUrl[0] + "/send_session_data",
                data: {
                    'complains': complains, // this value is not storing in DB
                    'diagnosis': diagnosis,
                    'investigation': investigation,
                    'medicine': medicine,
                    'appointmentId': appointmentId,
                    'followUpDate': followUpDate,
                    'spentHour': spentHour
                },
                success: function (result) {
                    // alert(result);
                },
            });
        }
        stopRTCrecord();
        sessionStorage.clear();
        window.location.href = baseUrl[0] + '/dashboard';
    }
};

// send an initial message on open data channel
connection.onopen = function (event) {
    connection.send('1stmsgonopendatachannel');
};

// send message function
function sendMessage(e) {
    if (e.type == "click" || e.keyCode == 13) {
        // console.log(sessionStorage.getItem("nickname"))
        var textmsg = document.getElementById("message");
        var textmsgRes = document.getElementById("messageRes");
        var messageArray = {};
        // console.log(textmsg.value)
        var time = getTime("chat");
        if (textmsg.value != "") {
            var chatMsg = "<div class='clearfix single-chat-div'><p class='sent-msg-para text-right float-right'><small>" +
                time +
                "</small><br>" +
                textmsg.value +
                "</p></div>";
            chatArray.push(chatMsg);
            sessionStorage.setItem("chatArray", JSON.stringify(chatArray));
            // console.log(chatArray);
            $("#chatBox").append(chatMsg);
            $("#chatBox").animate({
                scrollTop: $("#chatBox").get(0).scrollHeight,
            },
                1000
            );
            messageArray = {
                msg: textmsg.value,
                username: sessionStorage.getItem("nickname"),
                // msgRes: textmsgRes.value,
            };
            // console.log(messageArray);
            connection.send(messageArray);
            textmsg.value = null;
        }
        if (textmsgRes.value != "") {
            var chatMsg = "<div class='clearfix single-chat-div'><p class='sent-msg-para text-right float-right'><small>" +
                time +
                "</small><br>" +
                textmsgRes.value +
                "</p></div>";
            chatArray.push(chatMsg);
            sessionStorage.setItem("chatArray", JSON.stringify(chatArray));
            // console.log(chatArray);
            $("#chatBoxResponsive").append(chatMsg);
            $("#chatBoxResponsive").animate({
                scrollTop: $("#chatBoxResponsive").get(0).scrollHeight,
            },
                1000
            );
            messageArray = {
                msg: textmsgRes.value,
                username: sessionStorage.getItem("nickname"),
                // msgRes: textmsgRes.value,
            };
            // console.log(messageArray);
            connection.send(messageArray);
            textmsgRes.value = null;
        }
    }
}

// on message arrival append the message to chat box
connection.onmessage = function (event) {
    // console.log(event.data);
    // sync data for canvas
    designer.syncData(event.data);
    if (event.data.msg != undefined) {
        var msg = event.data.msg;
        var time = getTime("chat");
        var chatMsg = "<div class='clearfix single-chat-div'><p class='received-msg-para text-left float-left'><small>" +
            time +
            " <b class='text-uppercase'>" +
            event.data.username +
            "</b></small><br>" +
            msg +
            "</p></div>";
        chatArray.push(chatMsg);
        sessionStorage.setItem("chatArray", JSON.stringify(chatArray));
        // console.log(chatArray);
        $("#chatBox").append(chatMsg);
        $("#chatBox").animate({
            scrollTop: $("#chatBox").get(0).scrollHeight,
        },
            1000
        );
        $("#chatBoxResponsive").append(
            "<div class='clearfix single-chat-div'><p class='received-msg-para text-left float-left'><small>" +
            time +
            " <b class='text-uppercase'>" +
            event.data.username +
            "</b></small><br>" +
            msg +
            "</p></div>"
        );
        $("#chatBoxResponsive").animate({
            scrollTop: $("#chatBoxResponsive").get(0).scrollHeight,
        },
            1000
        );
        // console.log($("#chatBox").text());
        // var msgToStore = time + ": " + event.data.username + "\n" + event.data.msg + "\n";
    }
};

// open chat on mobile view
function openChatModal() {
    $(".chat-box-responsive").css("display", "block");
    var urlReplace = "#" + $(this).attr("id"); // make the hash the id of the modal shown
    history.pushState(null, null, urlReplace); // push state that hash into the url
};

// close chat on mobile view
$("#chatClose").click(function () {
    $(".chat-box-responsive").css("display", "none");
});

// close modal on back button click for mobile view
$(".modal").on("shown.bs.modal", function () {
    // any time a modal is shown
    var urlReplace = "#" + $(this).attr("id"); // make the hash the id of the modal shown
    history.pushState(null, null, urlReplace); // push state that hash into the url
});

// If a pushstate has previously happened and the back button is clicked, hide any modals.
$(window).on("popstate", function () {
    $(".modal").modal("hide");
    $(".chat-box-responsive").css("display", "none");
});

// mute or unmute own microphone
$("#mute-btn").click(function () {
    if (muteState == true) {
        var localStream = connection.attachStreams[0];
        localStream.unmute("audio");
        muteState = false;
        $("#mute-btn > i").removeClass("fa-volume-up");
        $("#mute-btn > i").addClass("fa-volume-mute");
    } else {
        var localStream = connection.attachStreams[0];
        localStream.mute("audio");
        muteState = true;
        $("#mute-btn > i").removeClass("fa-volume-mute");
        $("#mute-btn > i").addClass("fa-volume-up");
    }
});

// append whiteboard/cxanvas in the view
designer.appendTo(document.getElementById("widget-container"));

// send canvas data to remote user
designer.addSyncListener(function (data) {
    // console.log(data);
    connection.send(data);
});

// open canvas function
function openCanvas() {
    $("#widget-container").css({ "visibility": "visible", "z-index": "999999999" });
    $("#widget-container>iframe").css({ "top": "50%", "opacity": "1" });
    $("#widget-container>button").css({ "top": "15px" })
}

// hide canvas function
function closeCanvas() {
    $("#widget-container>iframe").css({ "top": "-50%", "opacity": "0" });
    $("#widget-container>button").css({ "top": "-35px" })
    setTimeout(function () {
        $("#widget-container").css({ "visibility": "hidden", "z-index": "-999999999" });
    }, 300);
}

// open end user history to doctor/patient for mobile view
function openHistory() {
    $("#paitentHistory").css("left", "0");
}

// close end user history for doctor/patient for mobile view
function closeHistory() {
    $("#paitentHistory").css("left", "-300px");
}

// disable previous date selection in follow up data field
$(document).ready(function () {
    var dtToday = new Date();
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if (month < 10)
        month = '0' + month.toString();
    if (day < 10)
        day = '0' + day.toString();
    var maxDate = year + '-' + month + '-' + day;
    $('#followUpDate').attr('min', maxDate);
})

// disable/enable console output for RTCMultiConnection() console.log()
connection.enableLogs = false;
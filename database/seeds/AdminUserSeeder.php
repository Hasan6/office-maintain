<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Role;
use App\User;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = User::create([
            'name' => 'Golam Kibria Papel',
            'email' => 'admin@gmail.com',
            'phone' => '01780208855',
            'gender' => 'Male',
            'age' => '30',
            'is_active' => 1,
            'password' => bcrypt('Admin@2020'),
        ]);

        // $user2 = User::create([
        //     'name' => 'Test Moderator',
        //     'email' => 'moderator@gmail.com',
        //     'phone' => '01780208850',
        //     'gender' => 'Male',
        //     'password' => bcrypt('Moderator@2020'),
        // ]);

        // $user3 = User::create([
        //     'name' => 'Test Doctor',
        //     'email' => 'doctor@gmail.com',
        //     'phone' => '01780208851',
        //     'gender' => 'Male',
        //     'password' => bcrypt('Doctor@2020'),
        // ]);

        $role1 = Role::create([
            'slug' => 'super-admin',
            'name' => 'Super Admin',           
        ]);

        $role2 = Role::create([
            'slug' => 'admin',
            'name' => 'Moderator',           
        ]);

        $role3 = Role::create([
            'slug' => 'power-user',
            'name' => 'PM',           
        ]);

        $role4 = Role::create([
            'slug' => 'user',
            'name' => 'employee',           
        ]);

        $user1->roles()->attach($role1->id);
        // $user2->roles()->attach($role2->id);
        // $user3->roles()->attach($role3->id);


        $createTasks = new Permission();
		$createTasks->slug = 'dashboard';
		$createTasks->name = 'Dashboard';
		$createTasks->save();
        
        $createTasks->roles()->attach($role1->id);
		
    }
}

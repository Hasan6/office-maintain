@extends('layouts.backend.app')

@section('title','Chat')

@push('css')
<style>
    section.content {
        margin: 0 0 0 300px;
    }

    .ls-closed section.content {
        margin-left: 0px;
    }
</style>
@endpush

@section('content')
<div class="container-fluid bg-dark">
    <div class="row">
        <div class="col-md-9">
            <section class="call-bar pt-3 pb-3 clearfix">
                <!-- <button class="btn btn-primary btn-outline-primary" type="button" id="record-btn" onclick="record();">start record</button>
                <button class="btn btn-primary btn-outline-primary" type="button" id="stop-btn" onclick="stop();">stop record</button> -->
                <!-- <button class="btn call-btn" data-toggle="modal" data-target="#createRoomModal" id="createRoomModalBtn">Create Room</button>
                <button class="btn call-btn" data-toggle="modal" data-target="#joinRoomModal" id="joinRoomModalBtn">Join</button> -->
                <div class="local-video d-inline">
                    <!-- local video here -->
                </div>
                <!-- <button class="btn" data-toggle="modal" data-target="#userConnectedModal" id="userConnectedModalBtn"></button> -->
                <div id="duration">
                    <!-- duration shows up here -->
                </div>
                <!-- <button id="testSocket">testSocket</button> -->
                <div class="float-right text-right room-id-div">
                    <small>Room ID</small>
                    <div id="roomIdDiv">NO ROOM JOINED</div>
                </div>
                <button class="btn btn-link float-right" data-toggle="tooltip" onclick="openCanvas();" data-placement="bottom" title="Whiteboard"><i class="fas fa-chalkboard"></i></button>
                <button class="btn btn-link float-right" id="openHistoryBtn" data-toggle="tooltip" onclick="openHistory();" data-placement="bottom" title="Paitent's History"><i class="fas fa-heartbeat"></i></button>
                <button class="btn btn-link float-right" id="openHistoryBtn" data-toggle="tooltip" onclick="openRx();" data-placement="bottom" title="Write Prescription"><i class="fas fa-prescription"></i></button>
            </section>
            <!-- video container -->
            <section id="videoContainer">
                <!-- video elements here -->
                <div class="bottom-call-bar">
                    <button class="btn btn-info" id="mute-btn"><i class="fas fa-volume-up"></i></button>
                    <button class="btn btn-danger" id="leave-btn" onclick="leaveChat();"><i class="fas fa-phone-slash"></i></button>
                    <button class="btn btn-info" id="chat-btn" onclick="openChatModal();"><i class="far fa-comments"></i></button>
                </div>
            </section>
        </div>
        <div class="col-md-3 chat-box-holder">
            <section class="doctor-field-holder">
                <div class="doctor-field">
                    <label for="complains">C/C</label>
                    <textarea class="mt-3" onkeyup="storeInSession(event);" name="complains" id="complains" placeholder="New complains..." autofocus></textarea>
                    <!-- <i class="fas fa-microphone"></i> -->
                </div>
                <div class="doctor-field">
                    <label for="diagnosis">Diagnosis</label>
                    <textarea class="mt-3" onkeyup="storeInSession(event);" name="diagnosis" id="diagnosis" placeholder="New Diagnosis..."></textarea>
                    <!-- <i class="fas fa-microphone"></i> -->
                </div>
                <div class="doctor-field">
                    <label for="investigation">Investigation</label>
                    <textarea class="mt-3" onkeyup="storeInSession(event);" name="investigation" id="investigation" placeholder="Investigation..."></textarea>
                    <!-- <i class="fas fa-microphone"></i> -->
                </div>
                <div class="doctor-field">
                    <label for="diagnosis"><i class="fas fa-prescription"></i></label>
                    <textarea class="mt-3" onkeyup="storeInSession(event);" name="medicine" id="medicine" placeholder="New Medicines..."></textarea>
                    <!-- <i class="fas fa-microphone"></i> -->
                </div>
                <div class="doctor-field">
                    <label for="followUpDate">Follow up date</label>
                    <input class="mt-3" onchange="storeInSession(event);" id="followUpDate" type="date">
                </div>
            </section>
            <section id="chatBox" class="mt-3 clearfix">
                <p class="text-center"><b>Dr. & Patient Conversation</b></p>
                <!-- chat here -->
            </section>
            <section class="in-group mt-3 mb-3">
                <input disabled type="text" id="message" class="d-inline" placeholder="Message here..." onkeyup="sendMessage(event);">
                <div class="d-inline">
                    <button disabled class="" type="button" id="sendMessageBtn" onclick="sendMessage(event);">Send</button>
                </div>
            </section>
        </div>
    </div>

    <!-- create room Modal -->
    {{-- <div class="modal fade" id="createRoomModal" tabindex="-1" role="dialog" aria-labelledby="createRoomModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createRoomModalLabel">Create a Room</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="createRoomCheckBtn">
                        <label class="form-check-label" for="createRoomCheckBtn">Create room with a random ID</label>
                    </div>
                    <div class="form-group" id="roomIdInputDiv">
                        <label for="roomId">Enter room ID</label>
                        <input type="text" class="form-control" id="roomId" name="roomId" placeholder="E.g. 'Chat Room' or '7thrg34t3ujg'">
                    </div>
                    <div class="form-group">
                        <label for="nicknameInput">Nickname</label>
                        <input type="text" class="form-control" id="nicknameInput" placeholder="Enter a nickname to show in chat">
                    </div>
                    <button type="button" class="btn btn-primary" id="createRoomBtn">Submit</button>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div> --}}

    <!-- join room Modal -->
    {{-- <div class="modal fade" id="joinRoomModal" tabindex="-1" role="dialog" aria-labelledby="joinRoomModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="joinRoomModalLabel">Join a Room</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="roomIdToJoin">Enter room ID</label>
                        <input type="text" class="form-control" id="roomIdToJoin" name="roomIdToJoin" placeholder="E.g. 'Chat Room' or '7thrg34t3ujg'">
                    </div>
                    <div class="form-group">
                        <label for="nicknameInputToJoin">Nickname</label>
                        <input type="text" class="form-control" id="nicknameInputToJoin" placeholder="Enter a nickname to show in chat">
                    </div>
                    <button type="button" class="btn btn-primary" id="joinRoomBtn">Submit</button>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div> --}}

    <!-- chat box visible in small screen -->
    <div class="chat-box-responsive">
        <button class="btn btn-outline-primary btn-primary" type="button" id="chatClose"><i class="fas fa-times"></i></button>
        <section id="chatBoxResponsive" class="mt-3 clearfix">
            <!-- chat here -->
        </section>
        <section class="in-group mt-3">
            <input disabled type="text" id="messageRes" class="d-inline" placeholder="Comments..." onkeyup="sendMessage(event);">
            <div class="d-inline">
                <button disabled class="btn btn-outline-primary btn-primary" type="button" id="sendMessageBtnRes" onclick="sendMessage(event);">Send</button>
            </div>
        </section>
    </div>

    <!-- connected user modal -->
    {{-- <div class="modal fade" id="userConnectedModal" tabindex="-1" role="dialog" aria-labelledby="userConnectedLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="userConnectedLabel">Connected user</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="user-list">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div> --}}

    <!-- canvas modal -->
    <div id="widget-container">
        <!-- canvas shows up here     -->
        <button type="button" class="close" onclick="closeCanvas();">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <!-- side navbar info for doctor and patient -->
    @if($ispatients == false)
    <div id="paitentHistory">
        <h4>Paitent Profile</h4>
            <img class="img-round" src="@if(is_null($appointment->users->image)) {{ url('storage/app/public/profile/no_profile.png') }} @else {{ url('storage/app/public/profile/'.$appointment->users->image) }} @endif" width="48" height="48" alt="User" />
        <p>
            <b>Name: </b>{{$appointment->users->name}}
        </p>
        <p>
            <b>Gender: </b>{{$appointment->users->gender}}
        </p>
        <p>
            <b>Age: </b>28 yrs
        </p>
        <p>
            <b>Previous Dr.: </b>
            @if ($appointment->previous_doctor == '')
            No previous Doctor found.
            @else
            {{$appointment->previous_doctor}}
            @endif
        </p>
        <p>
            <b>New Complain: </b> {{ $appointment->patient_symptoms }}
        </p>
        <p>
            <b>Next Appointments: </b>
            @if ($appointment->new_appointment == '')
            No next appointment scheduled.
            @else
            {{$appointment->new_appointment}}
            @endif
        </p>
        <p>
            <b>Diagnosis: </b>
            @if ($appointment->patient_symptoms == '')
            No previous data found.
            @else
            {{$appointment->patient_symptoms}}
            @endif
        </p>
        <p>
            <b>Medicine: </b>{{$appointment->prescribe_medicines}}
        </p>
        <button class="btn btn-outline-primary btn-primary" type="button" id="historyClose" onclick="closeHistory();"><i class="fas fa-times"></i></button>
    </div>
    @endif

    @if($ispatients == true)
    <div id="paitentHistory">
        <h4>Doctor Profile</h4>
        <img class="img-round" src="@if(is_null($appointment->users->image)) {{ url('storage/app/public/profile/no_profile.png') }} @else {{ url('storage/app/public/profile/'.$appointment->users->image) }} @endif" width="48" height="48" alt="User" />
        <p>
            <b>Name: </b>{{$doctorInfo->name}}
        </p>
        <p>
            <b>Email: </b>{{$doctorInfo->email}}
        </p>
        <button class="btn btn-outline-primary btn-primary" type="button" id="historyClose" onclick="closeHistory();"><i class="fas fa-times"></i></button>
    </div>
    @endif
</div>
@endsection

@push('js')
<script type="text/javascript">
    // set some global variable for custom_rtc.js
    @php
    echo "var iceServers = $result;";
    echo "var appointmentId = '$id';";
    echo "var roomId = '$room';";
    echo "var nickname = '$userName';";
    echo "var isPatients = '$ispatients';";
    @endphp
</script>
<!-- webRTC script -->
<script src="{{ asset('public/js/RTCMultiConnection.min.js')}}"></script>
<script src="{{ asset('public/js/socket.io.js')}}"></script>
<script src="{{ asset('public/js/RecordRTC.js')}}"></script>
<script src="{{ asset('public/js/webrtc-handler.js')}}"></script>
<script src="{{ asset('public/js/canvas-designer-widget.js')}}"></script>
<script src="{{ asset('public/js/custom_rtc.js')}}"></script>
@endpush

@extends('layouts.backend.app')
@if(auth()->check() && auth()->user()->hasRole('super-admin'))
    @section('title',"ADMIN DASHBOARD")
@elseif(auth()->check() && auth()->user()->hasRole('admin'))
    @section('title',"MODERATOR DASHBOARD")
@elseif(auth()->check() && auth()->user()->hasRole('power-user'))
    @section('title',"PM DASHBOARD")
@else
    @section('title',"Employee DASHBOARD")
@endif
@push('css')
<link href="{{asset('public/assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
<style>
table,th{
	font-size:small;	
}
</style>
@endpush
@section('content')
 
<div class="container-fluid">
   <div style="font-size: 20px; font-weight: bold;" class="text-center">Welcome  
   @if(auth()->check() && auth()->user()->hasRole('super-admin'))
        {{ Auth::user()->name }} ! 
    @endif 

    @if(auth()->check() && auth()->user()->hasRole('admin'))
        {{ Auth::user()->name }} !
    @endif 
       
    @if(auth()->check() && auth()->user()->hasRole('power-user'))
        {{ Auth::user()->name }} !
    @endif  

    @if(auth()->check() && auth()->user()->hasRole('user'))
        {{ Auth::user()->name }} !
    @endif 

    </div>

    @if(auth()->check() && auth()->user()->hasRole('super-admin'))
        <div class="block-header">
            <h2 class="text-left">ADMIN DASHBOARD</h2>       
        </div>
    @endif  


    @if(auth()->check() && auth()->user()->hasRole('admin'))
        <div class="block-header">
            <h2 class="text-left">MODERATOR DASHBOARD</h2>       
        </div>
    @endif    
    @if(auth()->check() && auth()->user()->hasRole('power-user'))
        <div class="block-header">
            <h2 class="text-left">PM'S DASHBOARD</h2>       
        </div>
    @endif  

    @if(auth()->check() && auth()->user()->hasRole('user'))
        <div class="block-header">
            <h2 class="text-left">EMPLOYEE'S DASHBOARD</h2>       
        </div>
    @endif 
  
    <!-- <div class="block-header">
        <h2>DASHBOARD</h2>
    </div> -->


    <!-- Widgets -->
    @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('power-user') || auth()->user()->hasRole('super-admin'))
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a href="#" style="text-decoration: none;">
            <div class="info-box bg-teal hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">supervised_user_circle</i>
                </div>
                <div class="content">
                    <div class="text">Total Employee</div>
                    <div class="number count-to" data-from="0" data-to="{{ $dashboard_info['total_patient'] }}" data-speed="1000" data-fresh-interval="20">{{ $dashboard_info['total_patient'] }}</div>
                </div>
            </div>
            </a>
        </div>        
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a href="#" style="text-decoration: none;">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">architecture</i>
                </div>
                <div class="content">
                    <div class="text">Projects</div>
                    <div class="number count-to" data-from="0" data-to="{{ $dashboard_info['new_patient'] }}" data-speed="1000" data-fresh-interval="20">{{ $dashboard_info['new_patient'] }}</div>
                </div>
            </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a href="#" style="text-decoration: none;">
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">event_available</i>
                </div>
                <div class="content">
                    <div class="text">Scheduled Meeting</div>
                    <div class="number count-to" data-from="0" data-to="{{ $dashboard_info['followup_patient'] }}" data-speed="15" data-fresh-interval="20">{{ $dashboard_info['followup_patient'] }}</div>
                </div>
            </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a href="#" style="text-decoration: none;">
            <div class="info-box bg-red hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">fact_check</i>
                </div>
                <div class="content">
                    <div class="text">WBS</div>
                    <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20">0</div>
                </div>
            </div>
        </a>
        </div>     
    </div>
    @endif
    <!-- Widgets End -->


    @if(auth()->check() && auth()->user()->hasRole('admin'))    
    <!-- MODERATOR Tab -->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <a class="btn btn-primary waves-effect" href="{{ route('users.create') }}">
                        <i class="material-icons">add</i>
                        <span>Add New Employee</span>
                    </a>
                </div>
                <div class="body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                        <li role="presentation" class="active"><a href="#patient" data-toggle="tab">Employee</a></li>   
                        <li role="presentation"><a href="#appointment" data-toggle="tab">Meetings</a></li>                                             
                        {{-- <li role="presentation"><a href="#followup_patient" data-toggle="tab">FOLLOWUP PATIENTS</a></li> --}}
                        <li role="presentation"><a href="#history" data-toggle="tab">WBS</a></li>
                        <li role="presentation"><a href="#doctors" data-toggle="tab">PMs</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="patient">                                   
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    
                                    <div class="card">
                                        <div class="header">
                                            <h2>ALL EMPLOYEES</h2>                            
                                        </div>
                                        <div class="body">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Name</th>
                                                            <th>Gender</th> 
                                                            <th>Email</th>
                                                            <th>Phone</th>                         
                                                            <th>WBS</th>
                                                            <!-- <th>Action</th> -->
                                                        </tr>
                                                    </thead>
                                                    <!-- <tfoot>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Name</th>
                                                            <th>Email</th>
                                                            <th>Role</th>
                                                            <th>Created At</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </tfoot> -->
                                                    <tbody>
                                                        @foreach($users as $key=>$user)
                                                        <tr>
                                                            <td>{{ $key + 1 }}</td>
                                                            <td>{{ $user->name }}</td>
                                                            <td>{{ $user->gender }}</td>
                                                            <td>{{ $user->email }}</td>
                                                            <td>{{ $user->phone }}</td>											
                                                            <td>
                                                            @if(!$user->appointments->isEmpty())
                                                                @foreach($user->appointments as $data)                                                                
                                                                    @if($loop->last)
                                                                        @if($data->isbooked == 1 && $data->isServiced == 0 && $data->isApproved == 1)
                                                                        <span class="badge bg-green">Booked</span>
                                                                        @elseif($data->isbooked == 1 && $data->isServiced == 0 && $data->isApproved == 0)   
                                                                        <span class="badge bg-red">Pending</span>                                                             
                                                                        @else
                                                                        <a href="{{ route('set_appointment',$user->id)}}" class="btn btn-info waves-effect">Book</a>	
                                                                        @endif  
                                                                    @endif                                                                   
                                                                @endforeach                                           
                                                            @else
                                                            <a href="{{ route('set_appointment',$user->id)}}" class="btn btn-info waves-effect">Book </a>	
                                                            @endif
                                                            </td>	
                                                           
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade " id="appointment">                            
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card">
                                        <div class="header">
                                            <h2>
                                            MEETINGS
                                                <span class="badge bg-blue"></span>
                                            </h2>
                                        </div>
                                        <div class="body">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Employee</th>
                                                            <th>Gender</th>
                                                            <th>Designation</th>
                                                            <th>Meeting Date</th>                                          
                                                            <th>Assigned PM</th>
                                                            <th>Project Name</th>
                                                            <th>Status</th>
                                                            <th>WBS</th>
                                                        </tr>
                                                    </thead>
                                                
                                                    <tbody>
                                                        @foreach($appoinments as $key=>$data)
                                                        <tr>
                                                            <td>{{ $key + 1 }}</td>
                                                            <td>{{ $data->users->name }}</td>
                                                            <td>{{ $data->users->gender }}</td>
                                                            <td>{{ $data->patient_symptoms }}</td>
                                                            <td><span style="font-weight:bold">{{ $data->visit_date }}</span> <br><span style="color:blue">{{ date('h:i:s A', strtotime($data->slots->start_time)) }} - {{ date('h:i:s A', strtotime($data->slots->end_time)) }}</span></td>
                                                            <td><span class="badge">{{ $data->doctors->name }}</span></td>  
                                                            <td>
                                                        
                                                                {{ $data->patient_type }} Patient
                                                                
                                                            </td>    
                                                            <td>
                                                            @if($data->isApproved == 1)
                                                            <span class="badge bg-green">Confirmed<span>
                                                            @else
                                                            <span class="badge bg-red">Pending<span>
                                                            @endif    
                                                            </td> 
                                                            
                                                            <td class="text-center">
                                                                <a href="{{ route('appointments.edit',$data->id)}}" class="btn btn-success waves-effect">
                                                                <i class="material-icons">edit</i>
                                                                </a>
                                                                <button class="btn btn-warning waves-effect"  type="button" onclick="deleteCategory('{{ $data->id }}')">
                                                                <i class="material-icons">clear</i>
                                                                </button>
                                                                <form id="delete-form-{{ $data->id }}" action="{{ route('appointments.destroy', $data->id) }}" method="POST" style="display:none">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                </form>
                                                                
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </div>                        
                       
                        {{-- <div role="tabpanel" class="tab-pane fade" id="followup_patient">                           
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card">
                                        <div class="header">
                                            <h2>
                                            FOLLOWUP PATIENTS
                                                <span class="badge bg-blue"></span>
                                            </h2>
                                        </div>
                                        <div class="body">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Patient</th>
                                                            <th>Gender</th>
                                                            <th>Symptoms</th>
                                                            <th>Medicine Prescribed</th>
                                                            <th>Assigned Doctor</th>
                                                            <th>LastVisit</th>   
                                                            <th>FollowupVisitDate</th>   
                                                            <th>Virtual Session</th>   
                                                        </tr>
                                                    </thead>
                                                
                                                    <tbody>
                                                        @foreach($followup_patient_list as $key=>$data)
                                                        <tr>
                                                            <td>{{ $key + 1 }}</td>
                                                            <td>{{ $data->users->name }}</td>
                                                            <td>{{ $data->users->gender }}</td>
                                                            <td>{{ $data->patient_symptoms }}</td>
                                                            <td>{{ $data->prescribe_medicines }}</td>
                                                            <td><span class="badge">{{ $data->doctors->name }}</span></td>
                                                            <td>{{ $data->visit_date }}</td>                                                            
                                                            <td>{{ $data->follow_up_visit_date }}</td>  
                                                            <td>
                                                             <a href="{{ route('set_appointment',$data->patient_id)}}" class="btn btn-info waves-effect">
                                                                <i class="material-icons">restore</i><span>Reschedule </span>
                                                                </a>
                                                            </td>                                                          
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}

                        <div role="tabpanel" class="tab-pane fade " id="history">                            
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card">
                                        <div class="header">
                                            <h2>
                                            WBS
                                                <span class="badge bg-blue"></span>
                                            </h2>
                                        </div>
                                        <div class="body">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Employee</th>
                                                            <th>Gender</th>
                                                            <th>Designation</th>
                                                            <th>Meeting Date</th>                                          
                                                            <th>Next Meeting Date</th>
                                                            <th>WBS</th>
                                                            <th>Assigned PM</th>
                                                            <th>SpentHour</th>
                                                            {{-- <th>PatientType</th> --}}
                                                        </tr>
                                                    </thead>
                                                
                                                    <tbody>
                                                        @foreach($appoinments as $key=>$data)
                                                        <tr>
                                                            <td>{{ $key + 1 }}</td>
                                                            <td>{{ $data->users->name }}</td>
                                                            <td>{{ $data->users->gender }}</td>
                                                            <td>{{ $data->patient_symptoms }}</td>
                                                            <td><span style="font-weight:bold">{{ $data->visit_date }}</span> <br><span style="color:blue">{{ date('h:i:s A', strtotime($data->slots->start_time)) }} - {{ date('h:i:s A', strtotime($data->slots->end_time)) }}</span></td>
                                                            <td>{{ $data->prescribe_medicines }}</td>
                                                            <td>{{ $data->follow_up_visit_date }}</td>
                                                            <td><span class="badge">{{ $data->doctors->name }}</span></td>  
                                                            <td>{{ $data->spent_hour }}</td>
                                                            {{-- <td>
                                                                {{ $data->patient_type }} Patient
                                                            </td>   --}}
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </div> 

                        <div role="tabpanel" class="tab-pane fade" id="doctors">
                            <!-- Doctor -->
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card">
                                        <div class="header">
                                            <h2>
                                                PM
                                                <span class="badge bg-blue"></span>
                                            </h2>
                                        </div>
                                        <div class="body">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Name</th>
                                                            <th>Email</th>
                                                            <th>Phone</th>
                                                            <th>Gender</th> 
                                                            <th>Status</th>                                                           
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>                           
                                                    <tbody>
                                                        @foreach($doctors as $key=>$user)
                                                        <tr>
                                                            <td>{{ $key + 1 }}</td>
                                                            <td>{{ $user->name }}</td>
                                                            <td>{{ $user->email }}</td>
                                                            <td>{{ $user->phone }}</td>
                                                            <td>{{ $user->gender }}</td>
                                                            <td>
                                                            @foreach($user->roles as $item)
                                                                {{ $item->name }}
                                                            @endforeach
                                                            </td>
                                                            <td>
                                                                <span class="badge bg-green">Active</span>                                       
                                                            </td>
                                                            
                                                            
                                                            <td class="text-center">
                                                                <a href="{{ route('users.edit',$user->id)}}" class="btn btn-info waves-effect">
                                                                    <i class="material-icons">edit</i>
                                                                </a>
                                                                <button class="btn btn-danger waves-effect"  type="button" onclick="deleteCategory('{{ $user->id }}')">
                                                                    <i class="material-icons">delete</i>
                                                                </button>
                                                                <form id="delete-form-{{ $user->id }}" action="{{ route('users.destroy', $user->id) }}" method="POST" style="display:none">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                </form>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Doctor -->
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# MODERATOR Tab -->
    @endif

    @if(auth()->check() && auth()->user()->hasRole('power-user'))    

    <!-- DOCTOR Tab -->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">                
                <div class="body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                        <li role="presentation" class="active"><a href="#appointment" data-toggle="tab">MEETINGS</a></li>
                        <li role="presentation"><a href="#patient" data-toggle="tab">MY EMPLOYEES</a></li>                        
                        <li role="presentation"><a href="#followup_patient" data-toggle="tab">SCHEDULED MEETINGS</a></li>                       
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="appointment">                            
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card">
                                        <div class="header">
                                            <h2>
                                            MEETINGS
                                                <span class="badge bg-blue"></span>
                                            </h2>
                                        </div>
                                        <div class="body">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Employee</th>
                                                            <th>Gender</th>
                                                            <th>Designation</th>
                                                            <th>Meeting Date</th>                                         
                                                            <th>Project Name</th>
                                                            <th>Virtual Session</th>
                                                            <!-- <th>Update</th> -->
                                                        </tr>
                                                    </thead>
                                                
                                                    <tbody>
                                                        @foreach($appoinments as $key=>$data)
                                                        <tr>
                                                            <td>{{ $key + 1 }}</td>
                                                            <td>{{ $data->users->name }}</td>
                                                            <td>{{ $data->users->gender }}</td>
                                                            <td>{{ $data->patient_symptoms }}</td>
                                                            <td><span style="font-weight:bold">{{ $data->visit_date }}</span> <br><span style="color:blue">{{ date('h:i:s A', strtotime($data->slots->start_time)) }} - {{ date('h:i:s A', strtotime($data->slots->end_time)) }}</span></td>
                                                            
                                                            <td>                                                            
                                                                {{ $data->patient_type }} Patient
                                                            </td>                                   
                                                        
                                                            
                                                            
                                                            <td class="text-center">
                                                                <a href="{{ route('chat', [$data->id, $data->room_id, $data->users->name]) }}" class="btn btn-info waves-effect">
                                                                    Start Session
                                                                </a>                                        
                                                            </td>
                                                            <td>
                                                            <button class="btn btn-warning waves-effect"  type="button" onclick="deleteCategory('{{ $data->id }}')">
                                                                    Cancel Appointment
                                                                </button>
                                                                <form id="delete-form-{{ $data->id }}" action="{{ route('appointments.destroy', $data->id) }}" method="POST" style="display:none">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                </form>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                           
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="patient">                                   
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    
                                    <div class="card">
                                        <div class="header">
                                            <h2>MY EMPLOYEES</h2>                            
                                        </div>
                                        <div class="body">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Name</th>
                                                            <th>Designation</th> 
                                                            <th>Email</th>
                                                            <th>Phone</th>                         
                                                            <th>Project Name</th>              
                                                            <!-- <th>Virtual Session</th> -->
                                                            <!-- <th>Action</th> -->
                                                        </tr>
                                                    </thead>
                                                   
                                                    <tbody>
                                                        @foreach($my_patients as $key=>$data)
                                                        <tr>
                                                            <td>{{ $key + 1 }}</td>
                                                            <td>{{ $data->users->name }}</td>
                                                            <td>{{ $data->users->gender }}</td>
                                                            <td>{{ $data->users->email }}</td>
                                                            <td>{{ $data->users->phone }}</td>											
                                                            <td>                                                             
                                                             {{ $data->patient_symptoms }}                                                          
                                                             </td>								
                                                            <!-- <td>                                                           
                                                                @if($data->isbooked == 1 && $data->isServiced == 0)
                                                                <span style="color:green">booked a virtual session</span>
                                                                @else
                                                                <a href="{{ route('set_appointment',$data->patient_id)}}" class="btn btn-info waves-effect">Create Session</a>	
                                                                @endif                                                                
                                                            </td>	 -->
                                                            
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        <div role="tabpanel" class="tab-pane fade" id="followup_patient">                           
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card">
                                        <div class="header">
                                            <h2>
                                            Scheduled Meetings
                                                <span class="badge bg-blue"></span>
                                            </h2>
                                        </div>
                                        <div class="body">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Employee</th>
                                                            <th>Gender</th>
                                                            <th>Designation</th>
                                                            <th>Task Assigned</th>
                                                            <th>Due Date</th>   
                                                            <th>Next Meeting Date</th>   
                                                            <th>Virtual Session</th>   
                                                        </tr>
                                                    </thead>
                                                
                                                    <tbody>
                                                        @foreach($followup_patient_list as $key=>$data)
                                                        <tr>
                                                            <td>{{ $key + 1 }}</td>
                                                            <td>{{ $data->users->name }}</td>
                                                            <td>{{ $data->users->gender }}</td>
                                                            <td>{{ $data->patient_symptoms }}</td>
                                                            <td>{{ $data->prescribe_medicines }}</td>                                                          
                                                            <td>{{ $data->visit_date }}</td>                                                            
                                                            <td>{{ $data->follow_up_visit_date }}</td>  
                                                            <td>
                                                             <a href="{{ route('set_appointment',$data->patient_id) }}" class="btn btn-info waves-effect">
                                                                <i class="material-icons">restore</i><span>Reschedule </span>
                                                                </a>
                                                            </td>                                                          
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# DOCTOR Tab -->
    @endif

    @if(auth()->check() && auth()->user()->hasRole('user'))  

    <!-- PATIENT Tab -->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">                
                <div class="body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                        {{-- <li role="presentation" class="active"><a href="#book" data-toggle="tab">BOOK APPOINTMENT</a></li> --}}
                        <li role="presentation" class="active"><a href="#appointment" data-toggle="tab">Meetings</a></li>
                        <li role="presentation"><a href="#my_history" data-toggle="tab">MY WBS</a></li> 
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        {{-- <div role="tabpanel" class="tab-pane fade in active" id="book">
                         @php 
                            $status = getPatientAppointmentStatus(auth()->user()->id);                         
                         @endphp

                         @if($status > 0)

                         <h5 class="text-success">You have requested for an Appointment. Please Click <strong>'APPOINTMENTS'</strong> from top menu for details</h5>

                         @else

                         <h4>Please click on 'Book Now' button to get an Appointment or call us at <span style="color:red">+8801780208855</span></h4>
                         <h3><a href="{{ route('set_appointment',auth()->user()->id) }}" class="btn btn-lg btn-success">Book Now</a></h3>
                         @endif                          
                        </div> --}}
                       
                        <div role="tabpanel" class="tab-pane fade in active" id="appointment">                            
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card">
                                        <div class="header">
                                            <h2>
                                            Meetings
                                                <span class="badge bg-blue"></span>
                                            </h2>
                                        </div>
                                        <div class="body">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Employee</th>
                                                            <th>Gender</th>
                                                            <th>Designation</th>
                                                            <th>Meeting Date</th>                                          
                                                            <th>Assigned PM</th>
                                                            <th>Status</th>
                                                            <th>Virtual Session</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                
                                                    <tbody>
                                                        @foreach($appoinments as $key=>$data)
                                                        <tr>
                                                            <td>{{ $key + 1 }}</td>
                                                            <td>{{ $data->users->name }}</td>
                                                            <td>{{ $data->users->gender }}</td>
                                                            <td>{{ $data->patient_symptoms }}</td>
                                                            <td>{{ $data->visit_date }} <br>
                                                            <span style="color:blue">{{ date('h:i:s A', strtotime($data->slots->start_time)) }} - {{ date('h:i:s A', strtotime($data->slots->end_time)) }} </span>
                                                            </td>
                                                            <td><span class="badge">{{ $data->doctors->name }}</span></td> 
                                                            <td>
                                                            @if($data->isApproved == 1)
                                                            <span class="badge bg-green">Confirmed<span>
                                                            @else
                                                            <span class="badge bg-red">Pending<span>
                                                            @endif
                                                            </td>
                                                            <td class="text-center">
                                                            @if($data->isApproved == 1)
                                                            <a href="{{ route('chat',  [$data->id, $data->room_id, $data->users->name]) }}" class="btn btn-info waves-effect">
                                                                    Join Session
                                                                </a> 
                                                            </td>
                                                            @else
                                                            <span class="text">N/A<span>
                                                            @endif
                                                            
                                                           
                                                            <td>
                                                            <button class="btn btn-danger waves-effect"  type="button" onclick="deleteCategory('{{ $data->id }}')">
                                                                    Cancel
                                                                </button>
                                                                <form id="delete-form-{{ $data->id }}" action="{{ route('appointments.destroy', $data->id) }}" method="POST" style="display:none">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                </form>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                      
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="my_history">                                   
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    
                                    <div class="card">
                                        <div class="header">
                                            <h2>MY WBS</h2>                            
                                        </div>
                                        <div class="body">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>PM</th>
                                                            <th>Meeting Date</th>                         
                                                            <th>Next Meeting Date</th>                         
                                                            <th>WBS</th>                         
                                                            {{-- <th>Medicine Prescribed</th> --}}
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                   
                                                    <tbody>
                                                        @foreach($my_history as $key=>$data)
                                                        <tr>
                                                            <td>{{ $key + 1 }}</td>
                                                            <td>{{ $data->doctors->name }}</td>
                                                            <td>{{ $data->visit_date }}</td>
                                                            <td>{{ $data->follow_up_visit_date }}</td>
                                                            <td>{{ $data->patient_symptoms }}</td>
                                                            {{-- <td>{{ $data->prescribe_medicines }}</td> --}}
                                                            <td>
                                                                <a href="{{ route('prescription_download',$data->id)}}" class="btn btn-info waves-effect" target="_blank">
                                                                    <i class="material-icons">file_download</i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# PATIENT Tab -->
    @endif


    @if(auth()->check() && auth()->user()->hasRole('super-admin'))
    <!-- Approval Requests -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Registration Approval Requests
                        <span class="badge bg-blue"></span>
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Gender</th>                                           
                                    <th>Role</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>                           
                            <tbody>
                                @foreach($approval_users as $key=>$user)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ $user->gender }}</td>
                                    <td>
                                    @foreach($user->roles as $item)
                                        {{ $item->name }}
                                    @endforeach
                                    </td>
                                    <td>
                                        <span class="badge bg-red">Pending</span>                                       
                                    </td>
                                    
                                    <td class="text-center">
                                        <button class="btn btn-warning waves-effect"  type="button" onclick="approveCategory('{{ $user->id }}')">
                                            Approve
                                        </button>
                                        <form id="approve-form-{{ $user->id }}" action="{{ route('approve_user', $user->id) }}" method="POST" style="display:none">
                                            @csrf
                                           
                                        </form>                                       
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Approval -->

    <!-- Moderator -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Moderators
                        <span class="badge bg-blue"></span>
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Gender</th>                                           
                                    <th>Role</th>
                                    <th>Status</th>
                                    <th>Approval</th>
                                    <th>Action</th>
                                </tr>
                            </thead>                           
                            <tbody>
                                @foreach($moderators as $key=>$user)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ $user->gender }}</td>
                                    <td>
                                    @foreach($user->roles as $item)
                                        {{ $item->name }}
                                    @endforeach
                                    </td>
                                    <td>
                                        <span class="badge bg-green">Active</span>                                       
                                    </td>
                                    
                                    <td class="text-center">
                                        <button class="btn btn-danger waves-effect"  type="button" onclick="pendingCategory('{{ $user->id }}')">
                                            Make Pending
                                        </button>
                                        <form id="pending-form-{{ $user->id }}" action="{{ route('pending_user', $user->id) }}" method="POST" style="display:none">
                                            @csrf
                                          
                                        </form>                                       
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('users.edit',$user->id)}}" class="btn btn-info waves-effect">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <button class="btn btn-danger waves-effect"  type="button" onclick="deleteModerator('{{ $user->id }}')">
                                            <i class="material-icons">delete</i>
                                        </button>
                                        <form id="delete-moderator-{{ $user->id }}" action="{{ route('users.destroy', $user->id) }}" method="POST" style="display:none">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Moderator -->
    
    <!-- Doctor -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        PMs
                        <span class="badge bg-blue"></span>
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Gender</th>                                           
                                    <th>Role</th>
                                    <th>Status</th>
                                    <th>Approval</th>
                                    <th>Action</th>
                                </tr>
                            </thead>                           
                            <tbody>
                                @foreach($doctors as $key=>$user)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ $user->gender }}</td>
                                    <td>
                                    @foreach($user->roles as $item)
                                        {{ $item->name }}
                                    @endforeach
                                    </td>
                                    <td>
                                        <span class="badge bg-green">Active</span>                                       
                                    </td>
                                    
                                    <td class="text-center">
                                        <button class="btn btn-danger waves-effect"  type="button" onclick="deleteCategory('{{ $user->id }}')">
                                            Make Pending
                                        </button>
                                        <form id="delete-form-{{ $user->id }}" action="{{ route('pending_user', $user->id) }}" method="POST" style="display:none">
                                            @csrf
                                           
                                        </form>                                       
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('users.edit',$user->id)}}" class="btn btn-info waves-effect">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <button class="btn btn-danger waves-effect"  type="button" onclick="deleteDoctor('{{ $user->id }}')">
                                            <i class="material-icons">delete</i>
                                        </button>
                                        <form id="delete-doctor-{{ $user->id }}" action="{{ route('users.destroy', $user->id) }}" method="POST" style="display:none">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Doctor -->

    @endif  
  
</div>

@endsection

@push('js')
    <script src="{{ asset('public/assets/backend/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('public/assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{ asset('public/assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('public/assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
    <script src="{{ asset('public/assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
    <script src="{{ asset('public/assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
    <script src="{{ asset('public/assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
    <script src="{{ asset('public/assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('public/assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
    <script src="{{ asset('public/assets/backend/js/pages/tables/jquery-datatable.js')}}"></script>
    
    <!-- <script src="{{ asset('public/assets/backend/js/pages/index.js') }}"></script> -->
      <!-- Jquery CountTo Plugin Js -->
    <script src="{{ asset('public/assets/backend/plugins/jquery-countto/jquery.countTo.js ')}}"></script>

    <!-- Morris Plugin Js -->
    <!-- <script src="{{ asset('public/assets/backend/plugins/raphael/raphael.min.js ')}}"></script>
    <script src="{{ asset('public/assets/backend/plugins/morrisjs/morris.js ')}}"></script> -->

    <!-- ChartJs -->
    <!-- <script src="{{ asset('public/assets/backend/plugins/chartjs/Chart.bundle.js ')}}"></script> -->

    <!-- Flot Charts Plugin Js -->
    <!-- <script src="{{ asset('public/assets/backend/plugins/flot-charts/jquery.flot.js ')}}"></script>
    <script src="{{ asset('public/assets/backend/plugins/flot-charts/jquery.flot.resize.js ')}}"></script>
    <script src="{{ asset('public/assets/backend/plugins/flot-charts/jquery.flot.pie.js ')}}"></script>
    <script src="{{ asset('public/assets/backend/plugins/flot-charts/jquery.flot.categories.js ')}}"></script>
    <script src="{{ asset('public/assets/backend/plugins/flot-charts/jquery.flot.time.js ')}}"></script> -->

    <!-- Sparkline Chart Plugin Js -->
    <!-- <script src="{{ asset('public/assets/backend/plugins/jquery-sparkline/jquery.sparkline.js ')}}"></script> -->
    
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    
    <script type="text/javascript">  
        function deleteCategory(id){
            const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
            title: 'Are you sure ?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes!',
            cancelButtonText: 'No!',
            reverseButtons: true
            }).then((result) => {
            if (result.value) {
                event.preventDefault();
                document.getElementById('delete-form-'+ id).submit();
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                // swalWithBootstrapButtons.fire(
                // 'Not Cancelled!',
                // )
            }
            })
        }      

        function deleteModerator(id){
            const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
            title: 'Are you sure to delete ?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes!',
            cancelButtonText: 'No!',
            reverseButtons: true
            }).then((result) => {
            if (result.value) {
                event.preventDefault();
                document.getElementById('delete-moderator-'+ id).submit();
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                // swalWithBootstrapButtons.fire(
                // 'Not Cancelled!',
                // )
            }
            })
        }

        function deleteDoctor(id){
            const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
            title: 'Are you sure to delete ?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes!',
            cancelButtonText: 'No!',
            reverseButtons: true
            }).then((result) => {
            if (result.value) {
                event.preventDefault();
                document.getElementById('delete-doctor-'+ id).submit();
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                // swalWithBootstrapButtons.fire(
                // 'Not Cancelled!',
                // )
            }
            })
        }

        function approveCategory(id){
            const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
            title: 'Are you sure to approve ?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes!',
            cancelButtonText: 'No!',
            reverseButtons: true
            }).then((result) => {
            if (result.value) {
                event.preventDefault();
                document.getElementById('approve-form-'+ id).submit();
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                // swalWithBootstrapButtons.fire(
                // 'Not Cancelled!',
                // )
            }
            })
        }

        function pendingCategory(id){
            const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
            title: 'Are you sure to pending ?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes!',
            cancelButtonText: 'No!',
            reverseButtons: true
            }).then((result) => {
            if (result.value) {
                event.preventDefault();
                document.getElementById('pending-form-'+ id).submit();
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                // swalWithBootstrapButtons.fire(
                // 'Not Cancelled!',
                // )
            }
            })
        }
       
    </script>
@endpush

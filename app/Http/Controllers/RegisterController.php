<?php

namespace App\Http\Controllers;

use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Mail;

class RegisterController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'phone' => 'required|unique:users',
            'gender' => 'required',
            'age' => 'required',
            'role_id' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);

        $moderator = User::with('roles')
            ->whereHas('roles', function ($q) {
                $q->where('slug', '=', 'admin');
            })
            ->where('is_deleted', 0)
            ->where('is_active', 1)
            ->get();

        set_time_limit(0);
        ini_set('max_execution_time', 180); //3 minutes

        // dd($request->all());
        if ($request->role_id == 4) {
            $html = '<html>
            <h3>Dear User,</h1>
            <h3>
            Thanks for Registering with Virtual Doctor.From now you can take advantage of our online medical services by logging into your account.
            </h3>
            <h3><a href="https://virtualdr.com.bd/login">Click Here</a> to Login your Dashboard!</h3>
            </html>';

            // dd('patient');
            $patient = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'age' => $request->age,
                'gender' => $request->gender,
                'is_active' => 1,
                'password' => bcrypt($request->password),
            ]);
            $to = $patient->email;
            $patient->roles()->attach($request->role_id);

            // send mail to patient for account creation
            Mail::send([], [], function ($message) use ($html, $to) {
                $message->from('contact@virtualdr.com.bd', 'Virtual Doctor');
                $message->to($to);
                $message->subject('Registration Successful!');
                $message->setBody($html,
                    'text/html');

            });

            // send mail to moderator for new patient creation
            foreach ($moderator as $data) {

                $mail_body = '<html>
                <h3>Dear ' . $data->name . ',</h1>
                <h3>A new patient has been registered to VirtualDoctor!</h3>
                <h3>Name: ' . $patient->name . '</h3>
                <h3>Email: ' . $patient->email . '</h3>
                <h3>Phone: ' . $patient->phone . '</h3>
                <h3><a href="https://virtualdr.com.bd/login">Click Here</a> to Login your Dashboard!</h3>
                </html>';

                $toModerator = $data->email;

                Mail::send([], [], function ($message) use ($mail_body, $toModerator) {
                    $message->from('contact@virtualdr.com.bd', 'Virtual Doctor');
                    $message->to($toModerator);
                    $message->subject('New Patient Registration!');
                    $message->setBody($mail_body,
                        'text/html');

                });
            }

        } else {
            $html = '<html>
            <h3>Dear User,</h1>
            <h3>Thanks for Registering with Virtual Doctor.You will receive another email from us confirming the account registration.</h3>
            <h3><a href="https://virtualdr.com.bd/login">Click Here</a> to Login your Dashboard!</h3>
            </html>';
            $data = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'age' => $request->age,
                'gender' => $request->gender,
                'password' => bcrypt($request->password),
            ]);
            $to = $data->email;

            $data->roles()->attach($request->role_id);

            Mail::send([], [], function ($message) use ($html, $to) {
                $message->from('contact@virtualdr.com.bd', 'Virtual Doctor');
                $message->to($to);
                $message->subject('Registration Successful!');
                $message->setBody($html,
                    'text/html');

            });

        }

        // dd($data);

        Toastr::success('Registration Completed Successfully :)', 'success');
        return redirect('/login');
    }

}

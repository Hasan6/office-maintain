<?php

namespace App\Http\Controllers\Admin;
use App\User;
use App\UsersRoles;
use App\Appointment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Auth;



class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {          
        $approval_users ='';
        $moderators ='';
        $doctors ='';
        $appoinments ='';
        $followup_patient_list='';
        $my_history='';
        $my_patients='';
        $dashboard_info='';
        


        if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('super-admin')){  
           $approval_users = User::latest()
                ->where('is_active', 0)
                ->where('is_deleted', 0)
                ->get();   
        
          $moderators = User::with('roles')
                ->whereHas('roles', function($q) {
                    $q->where('slug', '=', 'admin'); 
                })
                ->where('is_active', 1)
                ->where('is_deleted', 0)
                ->orderBy('created_at', 'desc')
                ->get();
             
          $doctors = User::with('roles')
                ->whereHas('roles', function($q) {
                    $q->where('slug', '=', 'power-user'); 
                })
                ->where('is_active', 1)
                ->where('is_deleted', 0)
                ->orderBy('created_at', 'desc')
                ->get();   

            $appoinments = Appointment::where('isbooked',1)
                    ->where('isServiced',0)
                    ->orderBy('created_at', 'asc')
                    ->get();


                    // total patients

            // $patients = Appointment::latest()
            //         ->where('isbooked',1)
            //         ->where('isServiced',1)
            //         ->get();

            $patients = User::with('roles')
                ->whereHas('roles', function($q) {
                    $q->where('slug', '=', 'user'); 
                })
                ->where('is_active', 1)
                ->where('is_deleted', 0)
                ->get();

            $total_patient = $patients->count();
            // dd($total_patient);

            // followup patients

            $followup = Appointment::latest()
                    ->where('isbooked',1)
                    ->where('isServiced',1)
                    ->whereNotNull('follow_up_visit_date')
                    ->get()->groupBy('patient_id');

            $followup_patient = $followup->count();

            $followup_patient_list = Appointment::latest()
            ->where('isbooked',1)
            ->where('isServiced',1)
            ->whereNotNull('follow_up_visit_date')
            ->get();
           

            // new patients

            $new_patient = Appointment::latest()
                    ->where('isbooked',1)
                    ->where('isServiced',0)
                    ->get();
            $new_patient = $new_patient->count();

            $dashboard_info = array('total_patient' => $total_patient, 'followup_patient' => $followup_patient, 'new_patient' => $new_patient);

        }

        if(Auth::user()->hasRole('power-user')){    
            $appoinments = Appointment::latest()
                    ->where('isbooked',1)
                    ->where('isServiced',0)
                    ->where('doctor_id',Auth::user()->id)
                    ->get();

            $patients = Appointment::latest()
                    ->where('isbooked',1)
                    ->where('isServiced',1)
                    ->where('doctor_id',Auth::user()->id)
                    ->get()->groupBy('patient_id');

            $my_patients = Appointment::latest()
                    ->where('isbooked',1)
                    ->where('isServiced',1)
                    ->where('doctor_id',Auth::user()->id)
                    ->get();

            $total_patient = $patients->count();  

            $followup_patient_list = Appointment::latest()
            ->where('isbooked',1)
            ->where('isServiced',1)
            ->where('doctor_id',Auth::user()->id)
            ->whereNotNull('follow_up_visit_date')
            ->get();


            // followup patients

            $followup = Appointment::latest()
                    ->where('isbooked',1)
                    ->where('isServiced',1)
                    ->where('doctor_id',Auth::user()->id)
                    ->whereNotNull('follow_up_visit_date')
                    ->get()->groupBy('patient_id');
            $followup_patient = $followup->count();

            // new patients

            $new_patient = Appointment::latest()
                    ->where('isbooked',1)
                    ->where('isServiced',0)
                    ->where('doctor_id',Auth::user()->id)
                    ->get();
                    
            $new_patient = $new_patient->count();

            $dashboard_info = array('total_patient' => $total_patient, 'followup_patient' => $followup_patient, 'new_patient' => $new_patient);
        }

        if(Auth::user()->hasRole('user')){    
            $appoinments = Appointment::latest()
                    ->where('isbooked',1)
                    ->where('isServiced',0)
                    ->where('patient_id',Auth::user()->id)
                    ->get(); 
           $my_history = Appointment::latest()
                ->where('isbooked',1)
                ->where('isServiced',1)
                ->where('patient_id',Auth::user()->id)
                ->get();

            $total_patient = 0;  

             // followup patients
            $followup_patient = 0;

            // new patients
            $new_patient = 0;

            $dashboard_info = array('total_patient' => $total_patient, 'followup_patient' => $followup_patient, 'new_patient' => $new_patient);
        }
        // dd($appoinments);
        $users = User::with('roles')
                   ->whereHas('roles', function($q) {
                       $q->where('slug', '=', 'user'); 
                   })
                   ->orderBy('created_at', 'desc')
                   ->get();

                   
        return view('admin.dashboard',compact('approval_users','followup_patient_list','moderators','doctors','users','appoinments','my_history','my_patients','dashboard_info'));   
          
    }

    
//     public function followup_patient_list()
//     {
//         $followup_patient_list='';
//         if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('super-admin')){     
//             $followup_patient_list = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',1)
//                     ->whereNotNull('follow_up_visit_date')
//                     ->get();


//                     // total patients

//             $patients = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',1)
//                     ->get();

//             $total_patient = $patients->count();


//             // followup patients

//             $followup = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',1)
//                     ->whereNotNull('follow_up_visit_date')
//                     ->get()->groupBy('patient_id');

//             $followup_patient = $followup->count();

//             // new patients

//             $new_patient = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',0)
//                     ->get();
//             $new_patient = $new_patient->count();

//             $dashboard_info = array('total_patient' => $total_patient, 'followup_patient' => $followup_patient, 'new_patient' => $new_patient);



//         }
//         if(Auth::user()->hasRole('power-user')){    
//             $followup_patient_list = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',1)
//                     ->where('doctor_id',Auth::user()->id)
//                     ->whereNotNull('follow_up_visit_date')
//                     ->get();

//             $patients = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',1)
//                     ->where('doctor_id',Auth::user()->id)
//                     ->get();

//             $total_patient = $patients->count();  


//             // followup patients

//             $followup = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',1)
//                     ->where('doctor_id',Auth::user()->id)
//                     ->whereNotNull('follow_up_visit_date')
//                     ->get();
//             $followup_patient = $followup->count();

//             // new patients

//             $new_patient = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',0)
//                     ->where('doctor_id',Auth::user()->id)
//                     ->get();
                    
//             $new_patient = $new_patient->count();

//             $dashboard_info = array('total_patient' => $total_patient, 'followup_patient' => $followup_patient, 'new_patient' => $new_patient);
//         }
//         if(Auth::user()->hasRole('user')){    
//             $followup_patient_list = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',1)
//                     ->where('patient_id',Auth::user()->id)
//                     ->whereNotNull('follow_up_visit_date')
//                     ->get(); 

//             $total_patient = 0;  

//              // followup patients
//             $followup_patient = 0;

//             // new patients
//             $new_patient = 0;

//             $dashboard_info = array('total_patient' => $total_patient, 'followup_patient' => $followup_patient, 'new_patient' => $new_patient);
//         }
//         // dd($appoinments);
//         $users = User::with('roles')
//                    ->whereHas('roles', function($q) {
//                        $q->where('slug', '=', 'user'); 
//                    })
//                    ->orderBy('created_at', 'desc')
//                    ->get();
                   
//         return view('admin.followup_patients',compact('users','followup_patient_list','dashboard_info'));   

//     }

//     public function new_patient_list()
//     {
//         $new_patient_list='';
//         if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('super-admin')){     
//             $new_patient_list = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',0)
//                     ->get();
//                     // total patients

//             $patients = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',1)
//                     ->get();

//             $total_patient = $patients->count();


//             // followup patients

//             $followup = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',1)
//                     ->whereNotNull('follow_up_visit_date')
//                     ->get()->groupBy('patient_id');

//             $followup_patient = $followup->count();

//             // new patients

//             $new_patient = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',0)
//                     ->get();
//             $new_patient = $new_patient->count();

//             $dashboard_info = array('total_patient' => $total_patient, 'followup_patient' => $followup_patient, 'new_patient' => $new_patient);



//         }
//         if(Auth::user()->hasRole('power-user')){    
//             $new_patient_list = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',0)
//                     ->where('doctor_id',Auth::user()->id)
//                     ->get();

//             $patients = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',1)
//                     ->where('doctor_id',Auth::user()->id)
//                     ->get();

//             $total_patient = $patients->count();  


//             // followup patients

//             $followup = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',1)
//                     ->where('doctor_id',Auth::user()->id)
//                     ->whereNotNull('follow_up_visit_date')
//                     ->get()->groupBy('patient_id');
//             $followup_patient = $followup->count();

//             // new patients

//             $new_patient = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',0)
//                     ->where('doctor_id',Auth::user()->id)
//                     ->get();
                    
//             $new_patient = $new_patient->count();

//             $dashboard_info = array('total_patient' => $total_patient, 'followup_patient' => $followup_patient, 'new_patient' => $new_patient);
//         }
//         if(Auth::user()->hasRole('user')){    
//             $new_patient_list = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',0)
//                     ->where('patient_id',Auth::user()->id)
//                     ->get(); 

//             $total_patient = 0;  

//              // followup patients
//             $followup_patient = 0;

//             // new patients
//             $new_patient = 0;

//             $dashboard_info = array('total_patient' => $total_patient, 'followup_patient' => $followup_patient, 'new_patient' => $new_patient);
//         }
//         // dd($appoinments);
//         $users = User::with('roles')
//                    ->whereHas('roles', function($q) {
//                        $q->where('slug', '=', 'user'); 
//                    })
//                    ->orderBy('created_at', 'desc')
//                    ->get();
                   
//         return view('admin.new_patients',compact('users','new_patient_list','dashboard_info'));   

//     } 


//     public function all_patient_list()
//     {
//         $all_patient_list='';
//         if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('super-admin')){     
//             $all_patient_list = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',1)
//                     ->get();


//                     // total patients

//             $patients = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',1)
//                     ->get();

//             $total_patient = $patients->count();


//             // followup patients

//             $followup = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',1)
//                     ->whereNotNull('follow_up_visit_date')
//                     ->get()->groupBy('patient_id');

//             $followup_patient = $followup->count();

//             // new patients

//             $new_patient = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',0)
//                     ->get();
//             $new_patient = $new_patient->count();

//             $dashboard_info = array('total_patient' => $total_patient, 'followup_patient' => $followup_patient, 'new_patient' => $new_patient);



//         }
//         if(Auth::user()->hasRole('power-user')){    
//             $all_patient_list = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',1)
//                     ->where('doctor_id',Auth::user()->id)
//                     ->get();

//             $patients = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',1)
//                     ->where('doctor_id',Auth::user()->id)
//                     ->get()->groupBy('patient_id');

//             $total_patient = $patients->count();  


//             // followup patients

//             $followup = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',1)
//                     ->where('doctor_id',Auth::user()->id)
//                     ->whereNotNull('follow_up_visit_date')
//                     ->get()->groupBy('patient_id');
//             $followup_patient = $followup->count();

//             // new patients

//             $new_patient = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',0)
//                     ->where('doctor_id',Auth::user()->id)
//                     ->get();
                    
//             $new_patient = $new_patient->count();

//             $dashboard_info = array('total_patient' => $total_patient, 'followup_patient' => $followup_patient, 'new_patient' => $new_patient);
//         }
//         if(Auth::user()->hasRole('user')){    
//             $all_patient_list = Appointment::latest()
//                     ->where('isbooked',1)
//                     ->where('isServiced',1)
//                     ->where('patient_id',Auth::user()->id)
//                     ->get(); 

//             $total_patient = 0;  

//              // followup patients
//             $followup_patient = 0;

//             // new patients
//             $new_patient = 0;

//             $dashboard_info = array('total_patient' => $total_patient, 'followup_patient' => $followup_patient, 'new_patient' => $new_patient);
//         }
//         // dd($appoinments);
//         $users = User::with('roles')
//                    ->whereHas('roles', function($q) {
//                        $q->where('slug', '=', 'user'); 
//                    })
//                    ->orderBy('created_at', 'desc')
//                    ->get();
                   
//         return view('admin.all_patients',compact('users','all_patient_list','dashboard_info'));   

//     }

//     public function emergency()
//     {
//         Toastr::warning('Under Construction :)','Incompleted');
//         return redirect()->back();
//     }
}
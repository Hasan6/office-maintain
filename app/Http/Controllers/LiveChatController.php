<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Appointment;
use App\User;
use Auth;

class LiveChatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function chat($id, $room, $name)
    {
        $appointment = Appointment::find($id);
        $ch = curl_init();
        $url = "https://teleassiststunturn.herokuapp.com/";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        $result = curl_exec($ch);
        if (Auth::check() && Auth::user()->hasRole('power-user')) {
            $ispatients = false;
        } else {
            $ispatients = true;
        }
        if (Auth::check()){
            // dd(Auth::user()->name);
            $userName = Auth::user()->name;
        }
        $doctorInfo = User::find($appointment->doctor_id);
        // dd($result);
        return view('admin.chat', compact('result', 'id', 'room', 'userName', 'appointment', 'ispatients', 'doctorInfo'));
    }

    public function send_session_data(Request $request)
    {
        $appointmentId = $request->appointmentId;
        $patient_symptoms = $request->diagnosis;
        $prescribe_medicines = $request->medicine;
        $follow_up_visit_date = $request->followUpDate;
        $spent_hour = $request->spentHour;
        $cc = $request->complains;
        $investigation	 = $request->investigation;
        $appointment = Appointment::find($appointmentId);
        $appointment->where('id', $appointmentId)->update(array(
            'prescribe_medicines' => $prescribe_medicines,
            'patient_symptoms' => $patient_symptoms,
            'isServiced' => 1,
            'follow_up_visit_date' => $follow_up_visit_date,
            'spent_hour' => $spent_hour,
            'cc' => $cc,
            'investigation' => $investigation,
        ));
        return "success";
    }
}
